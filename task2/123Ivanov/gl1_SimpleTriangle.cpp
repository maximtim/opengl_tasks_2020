#include <iostream>

#include <gl/buffer.h>
#include <gl/vertex_array.h>
#include <gl/shader_program.h>
#include <app/application.h>
#include <gl/render.h>
#include <gl/texture.h>
#include <3d/3dapplication.h>

class MyApp : public Abstract3dApplication {
public:
    MyApp() : Abstract3dApplication(800, 600, "rectangle") {
        VertexBufferLayout layout;
        layout.AddAttribute<AttributeType::kFloat>(3, false);
        layout.AddAttribute(2, false);
        vertex_array_.AddVertexBuffer(vertex_buffer_, layout);

        texture_.Bind();
        program_.SetUniform("textur", 0);
    }

protected:
    void UpdateState() override {
        Abstract3dApplication::UpdateState();
        if (red_ >= 0.9 || red_ <= 0.1) {
            step_ = -step_;
        }
        red_ += step_;
        program_.SetUniform("red", red_);
        program_.SetUniform("proj", camera_.GetViewProjectionMatrix());
    }

    void DrawFrame() override {
        render::Clear();
        render::Draw(vertex_array_, index_buffer_, program_);
    }

    void OnPress(Key key) override {
        if (key == Key::kEscape) {
            CloseWindow();
        }
    }

private:
    float red_ = 0.5;
    float step_ = 0.05;

    VertexArray vertex_array_;
    VertexBuffer vertex_buffer_{
        -1.5f, -1.5f, 0.f, 0.f, 0.f, 1.5f, -1.5f, 0.f, 0.f, 1.f, -1.5f, 1.5f, 0.f, 1.f, 0.f, 1.5f, 1.5f, 0.f, 1.f, 1.f,
    };
    IndexBuffer index_buffer_{0u, 1u, 2u, 1u, 2u, 3u};

    ShaderProgram program_{"123IvanovData2/simple.vert", "123IvanovData2/simple.frag"};

    Texture texture_{"699TimokhinData2/sand.jpg"};
};

int main() {
    MyApp app;
    app.Run();
    return 0;
}