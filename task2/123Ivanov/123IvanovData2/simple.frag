#version 330 core

in vec4 position;
in vec2 tex_coord;
out vec4 fragColor;

uniform float red;
uniform sampler2D textur;

void main()
{
	vec4 color = vec4(red, 0.3, 0.8, 1);
	vec4 texColor = texture(textur, tex_coord);
	fragColor = 0.5 * (color + texColor);
}
