/*
Простейший вершинный шейдер для первого семинара. Подробности - в семинаре №3
*/

#version 330 core

layout(location=0) in vec3 vertexPosition;
layout(location=1) in vec2 texCoord;

uniform mat4 proj;

out vec2 tex_coord;

void main()
{
   gl_Position = proj * vec4(vertexPosition, 1.0);
   tex_coord = texCoord;
}