#version 330 core

in vec4 v_Mask;
in vec3 v_Position;
in vec3 v_Normal;

out vec3 fragColor;

uniform sampler2D sand;
uniform sampler2D grass;
uniform sampler2D rock;
uniform sampler2D snow;

uniform vec3 lightDir;
uniform vec3 lightColor;

uniform vec3 cameraPos;

void main()
{
    vec4 sandColor = texture(sand, v_Position.xy * 2.);
    vec4 grassColor = texture(grass, v_Position.xy / 4.);
    vec4 rockColor = texture(rock, v_Position.xy / 5.);
    vec4 snowColor = texture(snow, v_Position.xy);
    vec4 color = v_Mask.r * sandColor + v_Mask.g * grassColor + v_Mask.b * rockColor + v_Mask.a * snowColor;

    float ambientStrength = 0.3f;
    float specularStrength = dot(v_Mask, vec4(0.05, 0.2, 0.3, 0.7));;

    vec4 ambient = vec4(ambientStrength * lightColor, 1.f);
    vec4 diffuse = vec4(lightColor * max(dot(normalize(v_Normal), normalize(lightDir)), 0.), 1.f);

    vec3 viewDir = normalize(cameraPos - v_Position);
    vec3 reflectDir = reflect(lightDir, v_Normal);
    vec4 specular = vec4(specularStrength * lightColor * pow(max(dot(viewDir, reflectDir), 0.0), 1.), 1.f);

    gl_FragColor = color * (ambient + diffuse + specular);
}
