#version 330 core

layout(location=0) in vec3 position;
layout(location=1) in vec3 normal;

uniform mat4 mvp;

out vec4 v_Mask;
out vec3 v_Position;
out vec3 v_Normal;

vec4 grad(float low, float height, float high, vec4 v1, vec4 v2) {
    if (height > high) {
        return v2;
    }
    if (height < low) {
        return v1;
    }
    float norm = high - low;
    float c = (high - height) / norm;
    return c * v1 + (1 - c) * v2;
}

void main()
{
    v_Position = position;
    v_Normal = normal;
    gl_Position = mvp * vec4(position, 1.0);

    if (position.z > 19.) {
        v_Mask = grad(2., position.z / 100. + abs(normal.z), 0., vec4(0., 0., 1., 0), vec4(0., 0., 0., 1));
    } else if (position.z > 15.) {
        v_Mask = grad(15., position.z, 19., vec4(0., 0., 1., 0),
        grad(2., 19. / 100. + abs(normal.z), 0., vec4(0., 0., 1., 0), vec4(0., 0., 0., 1)));
    } else if (position.z > 7.) {
        v_Mask = grad(0.7, abs(normal.z), 0.9, vec4(0., 0., 1., 0), vec4(0., 1., 0., 0.));
    } else if (position.z > 4.) {
        v_Mask = grad(5., position.z, 7., vec4(0., 1., 0., 0), vec4(0., 0., 1., 0.));
    } else {
        v_Mask = grad(0., position.z, 3., vec4(1., 0., 0., 0), vec4(0., 1., 0., 0.));
    }
}