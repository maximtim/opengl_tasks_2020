#include "terrain/terrain_app.h"

#include <iostream>

int main() {
    TerrainApp app;
    app.Run();
    return 0;
}