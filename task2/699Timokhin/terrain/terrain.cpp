#include "terrain.h"

#include <gl/render.h>

Terrain::Terrain(int seed) : seed_(seed) {
    InitIndices();
    InitVertices();
}

float Terrain::Height(float x, float y) {
    float height = relief_(x / kBaseSmoothness, y / kBaseSmoothness);

    if (height < kPondThreshold) {
        float pond_weigh = (kPondThreshold - height) / (kPondThreshold - kBaseBottom);
        height += pond_weigh * pond_noise_(x / kPondSmoothness, y / kPondSmoothness);
    } else if (height > kRockThreshold) {
        float rock_weigh = (height - kRockThreshold) / (kBaseTop - kRockThreshold);
        height += rock_weigh * rock_noise_(x / kRockSmoothness, y / kRockSmoothness);
    }

    // dip borders to the sea
    float distance_to_edge = std::min(std::min(std::abs(x - kLimit), std::abs(x + kLimit)),
                                      std::min(std::abs(y - kLimit), std::abs(y + kLimit)));
    if (distance_to_edge < kBorder) {
        float pond_weigh = distance_to_edge / kBorder;
        return (height + 1.f) * pond_weigh - 1.f;
    }

    return height;
}

void Terrain::Draw(ShaderProgram& shader) {
    sand_.Bind(0);
    shader.SetUniform("sand", 0);
    grass_.Bind(1);
    shader.SetUniform("grass", 1);
    rock_.Bind(2);
    shader.SetUniform("rock", 2);
    snow_.Bind(4);
    shader.SetUniform("snow", 4);
    render::Draw(vertex_array_, indices_, shader);
}

void Terrain::InitIndices() {
    std::vector<unsigned int> indices;
    indices.reserve(3 * 2 * (kSidePoints - 1) * (kSidePoints - 1));

    for (int i = 0; i < kSidePoints - 1; ++i) {
        for (int j = 0; j < kSidePoints - 1; ++j) {
            unsigned int top_left = kSidePoints * i + j, top_right = kSidePoints * i + j + 1,
                         bottom_left = kSidePoints * (i + 1) + j, bottom_right = kSidePoints * (i + 1) + j + 1;
            indices.push_back(top_left);
            indices.push_back(top_right);
            indices.push_back(bottom_left);

            indices.push_back(top_right);
            indices.push_back(bottom_left);
            indices.push_back(bottom_right);
        }
    }

    indices_ = IndexBuffer(indices);
}

void Terrain::InitVertices() {
    const static float eps = 1e-3;
    float height = 0., normal_x = 0., normal_y = 0., normal_z = 0.;
    auto get_normal = [this, &height, &normal_x, &normal_y, &normal_z](float x, float y) {
        height = Height(x, y);
        normal_x = (Height(x + eps, y) - height) / eps;
        normal_y = (Height(x, y + eps) - height) / eps;

        float length = sqrtf(normal_x * normal_x + normal_y * normal_y + 1);
        normal_x /= length;
        normal_y /= length;
        normal_z = -1.f / length;
    };

    std::vector<float> vertices;
    vertices.reserve(6 * kSidePoints * kSidePoints);

    const float step = 2 * kLimit / static_cast<float>(kSidePoints - 1);
    float x = -kLimit, y = -kLimit;
    for (int i = 0; i < kSidePoints; ++i) {
        for (int j = 0; j < kSidePoints; ++j) {
            get_normal(x, y);

            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(height);

            vertices.push_back(normal_x);
            vertices.push_back(normal_y);
            vertices.push_back(normal_z);
            x += step;
        }
        y += step;
        x = -kLimit;
    }

    VertexBufferLayout layout;
    layout.AddAttribute(3, false);
    layout.AddAttribute(3, false);

    vertices_ = VertexBuffer(vertices);
    vertex_array_.AddVertexBuffer(vertices_, layout);
}
