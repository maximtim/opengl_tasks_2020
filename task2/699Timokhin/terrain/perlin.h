#pragma once

#include <vector>
#include <random>

class PerlinNoise {
public:
    explicit PerlinNoise(float bottom = -1., float top = 1., int seed = 42);
    void Randomize();
    float operator()(float x, float y, float z);
    float operator()(float x, float y);
    float operator()(float x);

private:
    float Normalize(float t);
    float Grad(uint8_t hash, float x, float y, float z);
    static float Fade(float t);
    static float Lerp(float w, float a, float b);

private:
    const float bottom_;
    const float top_;
    uint8_t perm_[512];

    std::mt19937 random_gen_;
};

class OctaveNoise {
public:
    explicit OctaveNoise(float bottom = -1., float top = 1., int octaves = 4, int seed = 1234);
    void Randomize();
    float operator()(float x, float y, float z);
    float operator()(float x, float y);
    float operator()(float x);

private:
    PerlinNoise noise_;
    int octaves_;
};