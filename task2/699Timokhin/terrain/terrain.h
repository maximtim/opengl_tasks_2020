#pragma once

#include "perlin.h"

#include <gl/shader_program.h>
#include <gl/vertex_array.h>
#include <gl/texture.h>

#include <functional>

class Terrain {
public:
    explicit Terrain(int seed = 42);
    float Height(float x, float y);
    void Draw(ShaderProgram& shader);

    constexpr static const float kLimit = 40.f;
    constexpr static const float kBorder = 5.f;

private:
    void InitIndices();
    void InitVertices();

private:
    static const int kSidePoints = 2000;

    constexpr static const float kBaseBottom = -5.;
    constexpr static const float kBaseTop = 15.;
    constexpr static const float kBaseSmoothness = 20.f;

    constexpr static const float kPondSmoothness = 2.f;
    constexpr static const float kRockSmoothness = 1.f;

    constexpr static const float kPondThreshold = 2.f;
    constexpr static const float kRockThreshold = 7.f;

    int seed_;
    PerlinNoise relief_{kBaseBottom, kBaseTop, seed_};
    OctaveNoise pond_noise_{-10., -5., 4, seed_ + 1};
    OctaveNoise rock_noise_{10., 20., 4, seed_ + 2};

    IndexBuffer indices_;
    VertexBuffer vertices_;
    VertexArray vertex_array_;
    Texture sand_{"699TimokhinData2/sand.jpg"};
    Texture grass_{"699TimokhinData2/grass.jpg"};
    Texture rock_{"699TimokhinData2/rock.jpg"};
    Texture snow_{"699TimokhinData2/snow.jpg"};
};