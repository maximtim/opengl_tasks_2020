#include "terrain.h"
#include "perlin.h"

#include <3d/3dapplication.h>
#include <gl/texture.h>

class TerrainApp : public Abstract3dApplication {
public:
    TerrainApp() : Abstract3dApplication(800, 600, "Terrain") {
        SetCameraRotationSpeed(0.03, 0.03);
        SetLightColor({1., 1., 1.});
        SetLightDirection({0., -0.5, -1.});
        // SetMoveSpeed(0.2);
    }

protected:
    void UpdateState() override {
        Abstract3dApplication::UpdateState();
        shader_.SetUniform("mvp", camera_.GetViewProjectionMatrix());
        shader_.SetUniform("cameraPos", camera_.GetPosition());
    }

    void UpdateCameraPosition() override {
        float move_speed = IsPressed(Key::kLeftShift) ? move_speed_ * 2 : move_speed_;

        if (IsPressed(Key::kW)) {
            auto direction_forward = camera_.GetDirection();
            direction_forward.z = 0.;
            direction_forward /= glm::length(direction_forward);
            camera_.Move(move_speed * direction_forward);
        }
        if (IsPressed(Key::kA)) {
            auto direction_left = -glm::cross(camera_.GetDirection(), camera_.GetUpDirection());
            direction_left.z = 0.;
            direction_left /= glm::length(direction_left);
            camera_.Move(move_speed * direction_left);
        }
        if (IsPressed(Key::kS)) {
            auto direction_back = -camera_.GetDirection();
            direction_back.z = 0.;
            direction_back /= glm::length(direction_back);
            camera_.Move(move_speed * direction_back);
        }
        if (IsPressed(Key::kD)) {
            auto direction_right = glm::cross(camera_.GetDirection(), camera_.GetUpDirection());
            direction_right.z = 0.;
            direction_right /= glm::length(direction_right);
            camera_.Move(move_speed * direction_right);
        }

        if (IsPressed(Key::kR)) {
            camera_.Move(glm::vec3(0, 0, move_speed_ * 5));
        } else if (IsPressed(Key::kF)) {
            camera_.Move(glm::vec3(0, 0, -move_speed_ * 5));
        } else {
            auto position = camera_.GetPosition();
            glm::vec3 shift{
                std::min(position.x, Terrain::kLimit) + std::max(position.x, -Terrain::kLimit) - 2.f * position.x,
                std::min(position.y, Terrain::kLimit) + std::max(position.y, -Terrain::kLimit) - 2.f * position.y, 0.};
            position += shift;
            shift.z = camera_height_ - position.z + ground_.Height(position.x, position.y);
            camera_.Move(shift);
        }
    }

    void DrawFrame() override {
        render::Clear();
        ground_.Draw(shader_);
    }

    void OnPress(Key key) override {
        Abstract3dApplication::OnPress(key);
        if (key == Key::kEscape) {
            CloseWindow();
        }
    }

private:
    void SetLightColor(const glm::vec3& color) {
        shader_.SetUniform("lightColor", color);
    }

    void SetLightDirection(const glm::vec3& direction) {
        shader_.SetUniform("lightDir", direction);
    }

private:
    ShaderProgram shader_{"699TimokhinData2/ground.vert", "699TimokhinData2/ground.frag"};
    Terrain ground_{55};
    const float camera_height_ = 0.5;
};