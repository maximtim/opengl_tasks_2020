#include "perlin.h"

#include <cmath>
#include <algorithm>

PerlinNoise::PerlinNoise(float bottom, float top, int seed) : bottom_(bottom), top_(top), random_gen_(seed) {
    Randomize();
}

void PerlinNoise::Randomize() {
    for (int i = 0; i < 256; ++i) {
        perm_[i] = static_cast<uint8_t>(i);
    }
    std::shuffle(std::begin(perm_), std::begin(perm_) + 256, random_gen_);
    for (int i = 0; i < 256; ++i) {
        perm_[i + 256] = perm_[i];
    }
}

float PerlinNoise::operator()(float x, float y, float z) {
    const int xd = static_cast<int>(std::floor(x)) & 255;
    const int yd = static_cast<int>(std::floor(y)) & 255;
    const int zd = static_cast<int>(std::floor(z)) & 255;

    x -= std::floor(x);
    y -= std::floor(y);
    z -= std::floor(z);

    const float u = Fade(x);
    const float v = Fade(y);
    const float w = Fade(z);

    const int a = perm_[xd] + yd, aa = perm_[a] + zd, ab = perm_[a + 1] + zd;
    const int b = perm_[xd + 1] + yd, ba = perm_[b] + zd, bb = perm_[b + 1] + zd;

    return Normalize(
        Lerp(w,
             Lerp(v, Lerp(u, Grad(perm_[aa], x, y, z), Grad(perm_[ba], x - 1, y, z)),
                  Lerp(u, Grad(perm_[ab], x, y - 1, z), Grad(perm_[bb], x - 1, y - 1, z))),
             Lerp(v, Lerp(u, Grad(perm_[aa + 1], x, y, z - 1), Grad(perm_[ba + 1], x - 1, y, z - 1)),
                  Lerp(u, Grad(perm_[ab + 1], x, y - 1, z - 1), Grad(perm_[bb + 1], x - 1, y - 1, z - 1)))));
}

float PerlinNoise::operator()(float x, float y) {
    return operator()(x, y, 0.f);
}

float PerlinNoise::operator()(float x) {
    return operator()(x, 0.f);
}

float PerlinNoise::Normalize(float t) {
    return bottom_ + (t + 1.f) / 2.f * (top_ - bottom_);
}

float PerlinNoise::Grad(uint8_t hash, float x, float y, float z) {
    const std::uint8_t h = hash & 15;
    const float u = h < 8 ? x : y;
    const float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

float PerlinNoise::Fade(float t) {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

float PerlinNoise::Lerp(float w, float a, float b) {
    return a + w * (b - a);
}

OctaveNoise::OctaveNoise(float bottom, float top, int octaves, int seed)
    : noise_(bottom, top, seed), octaves_(octaves) {
}

void OctaveNoise::Randomize() {
    noise_.Randomize();
}

float OctaveNoise::operator()(float x, float y, float z) {
    float value = 0.f;
    float amp = 1.f;

    for (int i = 0; i < octaves_; ++i) {
        value += noise_(x, y, z) * amp;
        x *= 2;
        y *= 2;
        z *= 2;
        amp /= 2;
    }

    return value / (2.f - amp * 2.f);
}

float OctaveNoise::operator()(float x, float y) {
    return operator()(x, y, 0.f);
}

float OctaveNoise::operator()(float x) {
    return operator()(x, 0.f);
}