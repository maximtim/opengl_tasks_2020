#include "terrain.h"
#include "perlin.h"

#include <3d/3dapplication.h>

class TerrainApp : public Abstract3dApplication {
public:
    TerrainApp() : Abstract3dApplication(800, 600, "Terrain"), ground_(-40, 40, 2000, height_function_) {
        SetMoveSpeed(0.1);
        SetCameraRotationSpeed(0.03, 0.03);
    }

protected:
    void UpdateState() override {
        Abstract3dApplication::UpdateState();
        shader_.SetUniform("mvp", camera_.GetViewProjectionMatrix());
    }

    void DrawFrame() override {
        render::Clear();
        ground_.Draw(shader_);
    }

    void OnPress(Key key) override {
        Abstract3dApplication::OnPress(key);
        if (key == Key::kEscape) {
            CloseWindow();
        }
    }

private:
    ShaderProgram shader_{"699TimokhinData1/ground.vert", "699TimokhinData1/ground.frag"};
    PerlinNoise relief_{0., 1., 12};
    PerlinNoise plain_function_{-0.5, 0.5, 89};
    OctaveNoise pond_function_{-5., 0., 4, 32};
    OctaveNoise rock_function_{0., 8., 4, 48};

    const float denominator_ = 7.f;
    const float rock_threshold_ = 0.3f;
    std::function<float(float, float)> height_function_ = [this](float x, float y) {
      float relief = relief_(x / denominator_, y / denominator_);
      if (relief < rock_threshold_) {
          return 2.f * pond_function_(x, y) * (0.5f - relief) + 2.f * plain_function_(x, y) * relief;
      }
      return 2.f * rock_function_(x, y) * (relief - 0.5f) + 2.f * plain_function_(x, y) * (1.f - relief);
    };

    Terrain ground_;
};