#include "terrain.h"

#include <gl/render.h>

Terrain::Terrain(float bottom, float top, int points_per_side,
                 const std::function<float(float, float)>& height_function)
    : bottom_(bottom), top_(top), points_per_side_(points_per_side), height_function_(height_function) {
    InitIndices();
    InitVertices();
}

void Terrain::Draw(const ShaderProgram& shader) {
    render::Draw(vertex_array_, indices_, shader);
}

void Terrain::InitIndices() {
    std::vector<unsigned int> indices;
    indices.reserve(3 * 2 * (points_per_side_ - 1) * (points_per_side_ - 1));

    for (int i = 0; i < points_per_side_ - 1; ++i) {
        for (int j = 0; j < points_per_side_ - 1; ++j) {
            unsigned int top_left = points_per_side_ * i + j, top_right = points_per_side_ * i + j + 1,
                         bottom_left = points_per_side_ * (i + 1) + j,
                         bottom_right = points_per_side_ * (i + 1) + j + 1;
            indices.push_back(top_left);
            indices.push_back(top_right);
            indices.push_back(bottom_left);

            indices.push_back(top_right);
            indices.push_back(bottom_left);
            indices.push_back(bottom_right);
        }
    }

    indices_ = IndexBuffer(indices);
}

void Terrain::InitVertices() {
    const static float eps = 1e-3;
    float height = 0., normal_x = 0., normal_y = 0., normal_z = 0.;
    auto get_normal = [this, &height, &normal_x, &normal_y, &normal_z](float x, float y) {
        height = height_function_(x, y);
        normal_x = height_function_(x + eps, y) - height;
        normal_y = height_function_(x, y + eps) - height;

        float length = sqrtf(normal_x * normal_x + normal_y * normal_y + 1);
        normal_x /= length;
        normal_y /= length;
        normal_z = -1.f / length;
    };

    std::vector<float> vertices;
    vertices.reserve(3 * 2 * points_per_side_ * points_per_side_);

    const float step = (top_ - bottom_) / static_cast<float>(points_per_side_ - 1);
    float x = bottom_, y = bottom_;
    for (int i = 0; i < points_per_side_; ++i) {
        for (int j = 0; j < points_per_side_; ++j) {
            get_normal(x, y);
            vertices.push_back(x);
            vertices.push_back(y);
            vertices.push_back(height);
            vertices.push_back(normal_x);
            vertices.push_back(normal_y);
            vertices.push_back(normal_z);
            x += step;
        }
        y += step;
        x = bottom_;
    }

    VertexBufferLayout layout;
    layout.AddAttribute(3, false);
    layout.AddAttribute(3, false);

    vertices_ = VertexBuffer(vertices);
    vertex_array_.AddVertexBuffer(vertices_, layout);
}