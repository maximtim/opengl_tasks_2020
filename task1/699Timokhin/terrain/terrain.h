#pragma once

#include <gl/shader_program.h>
#include <gl/vertex_array.h>

#include <functional>

class Terrain {
public:
    Terrain(float bottom, float top, int points_per_side, const std::function<float(float, float)>& height_function);
    void Draw(const ShaderProgram& shader);

private:
    void InitIndices();
    void InitVertices();

private:
    float bottom_;
    float top_;
    int points_per_side_;
    const std::function<float(float, float)> height_function_;

    IndexBuffer indices_;
    VertexBuffer vertices_;
    VertexArray vertex_array_;
};