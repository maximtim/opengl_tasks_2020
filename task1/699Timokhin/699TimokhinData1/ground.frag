#version 330

in vec4 position;

out vec3 fragColor;

void main()
{
    float red = (position.z + 3.) / 6.;
    fragColor = vec3(red, 0.2, 1. - red);
}
