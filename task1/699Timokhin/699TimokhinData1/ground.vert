#version 330

in vec3 vertexPosition;

uniform mat4 mvp;

out vec4 position;

void main()
{
    position = vec4(vertexPosition, 1.0);
    gl_Position = mvp * position;
}