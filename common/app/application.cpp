#include "application.h"

#include <iostream>

AbstractApplication::AbstractApplication(int window_width, int window_heigth, std::string window_title)
    : window_width_(window_width), window_height_(window_heigth), window_title_(std::move(window_title)) {
    Init();
}

AbstractApplication::~AbstractApplication() {
    glfwTerminate();
}

void AbstractApplication::Run() {
    RunLoop();
}

void AbstractApplication::Init() {
    InitContext();
    InitGL();
    SetCallbacks();
}

void AbstractApplication::UpdateState() {
    glfwPollEvents();

    glfwGetFramebufferSize(window_, &window_width_, &window_height_);
    glViewport(0, 0, window_width_, window_height_);
}

bool AbstractApplication::IsPressed(Key key) {
    return glfwGetKey(window_, static_cast<int>(key)) == GLFW_PRESS;
}

void AbstractApplication::OnPress(Key key) {
}

void AbstractApplication::OnRelease(Key key) {
}

void AbstractApplication::CloseWindow() {
    glfwSetWindowShouldClose(window_, GL_TRUE);
}

void AbstractApplication::InitContext() {
    if (!glfwInit()) {
        std::cerr << "error: could not start GLFW3" << std::endl;
        exit(1);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window_ = glfwCreateWindow(window_width_, window_height_, window_title_.data(), nullptr, nullptr);
    if (!window_) {
        std::cerr << "error: could not open window with GLFW3" << std::endl;
        delete this;
        exit(1);
    }

    glfwSetWindowUserPointer(window_, this);
    glfwMakeContextCurrent(window_);
}

void AbstractApplication::InitGL() {
    glewExperimental = GL_TRUE;
    glewInit();
    glEnable(GL_DEPTH_TEST);
}

void AbstractApplication::SetCallbacks() {
    glfwSetKeyCallback(window_, AbstractApplication::KeyCallbackTrampoline);
}

void AbstractApplication::RunLoop() {
    while (!glfwWindowShouldClose(window_)) {
        UpdateState();
        DrawFrame();
        glfwSwapBuffers(window_);
    }
}

void AbstractApplication::KeyCallbackTrampoline(GLFWwindow* window, int key, int scancode, int action, int mods) {
    auto app = reinterpret_cast<AbstractApplication*>(glfwGetWindowUserPointer(window));
    switch (action) {
        case GLFW_PRESS:
            app->OnPress(static_cast<Key>(key));
            break;
        case GLFW_RELEASE:
            app->OnRelease(static_cast<Key>(key));
            break;
        default:;
    }
}
