#pragma once

#include <functional>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "gl/render.h"
#include "keys.h"

class AbstractApplication {
public:
    AbstractApplication(int window_width, int window_heigth, std::string window_title);
    virtual ~AbstractApplication();

    void Run();

protected:
    virtual void Init();

    virtual void UpdateState();
    virtual void DrawFrame() = 0;

    virtual bool IsPressed(Key key);
    virtual void OnPress(Key key);
    virtual void OnRelease(Key key);

    virtual void CloseWindow();

protected:
    GLFWwindow* window_;
    int window_width_ = 0;
    int window_height_ = 0;

private:
    void InitContext();
    void InitGL();
    void SetCallbacks();

    void RunLoop();

    static void KeyCallbackTrampoline(GLFWwindow* window, int key, int scancode, int action, int mods);

private:
    std::string window_title_;
};
