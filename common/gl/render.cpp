#include "render.h"

namespace render {
void Clear() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Draw(const VertexArray& vertices, const IndexBuffer& indices, const ShaderProgram& shader) {
    indices.Bind();
    vertices.Bind();
    shader.Bind();

    glDrawElements(GL_TRIANGLES, indices.GetSize(), GL_UNSIGNED_INT, nullptr);
}

}  // namespace render
