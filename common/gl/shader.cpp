#include "shader.h"

#include <fstream>
#include <iostream>

Shader::~Shader() {
    glDeleteShader(id_);
}

void Shader::FromString(const char* code) {
    glShaderSource(id_, 1, &code, nullptr);
    glCompileShader(id_);
    CheckCompilation();
}

void Shader::FromString(const std::string& code) {
    FromString(code.data());
}

void Shader::FromFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file) {
        std::cerr << "cannot open file " << filename << std::endl;
        exit(1);
    }

    file.seekg(0, std::ios_base::end);
    int file_size = file.tellg();
    file.seekg(0, std::ios_base::beg);

    std::string code(file_size, 0);
    file.read(code.data(), file_size);
    file.close();

    FromString(code);
}

unsigned int Shader::GetId() const {
    return id_;
}

ShaderType Shader::GetType() const {
    return type_;
}

std::string Shader::GetTypeAsString() {
    switch (GetType()) {
        case ShaderType::kVertex:
            return "vertex";
        case ShaderType ::kFragment:
            return "fragment";
    }
    return "";
}

void Shader::CheckCompilation() {
    int compile_status = GL_FALSE;
    glGetShaderiv(id_, GL_COMPILE_STATUS, &compile_status);
    if (compile_status != GL_TRUE) {
        int message_size;
        glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &message_size);

        std::string message(message_size, 0);
        glGetShaderInfoLog(id_, message_size, nullptr, message.data());

        std::cerr << GetTypeAsString() << " shader compilation failed:\n" << message << std::endl;
        exit(1);
    }
}
