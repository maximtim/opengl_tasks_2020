#pragma once

#include "vertex_array.h"
#include "buffer.h"
#include "shader_program.h"

namespace render {
void Clear();

void Draw(const VertexArray& vertices, const IndexBuffer& indices, const ShaderProgram& shader);

}  // namespace render