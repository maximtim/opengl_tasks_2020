#pragma once

#include <GL/glew.h>
#include <string>

class Texture {
public:
    Texture(const std::string& filepath);
    ~Texture();
    void Bind(unsigned int slot = 0) const;
    void Unbind() const;
private:
    unsigned int id_ = 0;
    int width_ = 0;
    int height_ = 0;

    unsigned char* image_{nullptr};
};