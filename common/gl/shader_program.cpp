#include "shader_program.h"

#include <iostream>
#include <glm/gtc/type_ptr.hpp>

ShaderProgram::ShaderProgram() {
    Bind();
}

ShaderProgram::ShaderProgram(std::shared_ptr<Shader> vertex_shader, std::shared_ptr<Shader> fragment_shader) {
    AttachShader(std::move(vertex_shader));
    AttachShader(std::move(fragment_shader));
    Link();
    Bind();
}

ShaderProgram::ShaderProgram(const std::string& vertex_shader_path, const std::string& fragment_shader_path) {
    auto vertex_shader = std::make_shared<Shader>(ShaderType::kVertex);
    vertex_shader->FromFile(vertex_shader_path);
    auto fragment_shader = std::make_shared<Shader>(ShaderType::kFragment);
    fragment_shader->FromFile(fragment_shader_path);

    AttachShader(vertex_shader);
    AttachShader(fragment_shader);
    Link();
    Bind();
}

void ShaderProgram::AttachShader(std::shared_ptr<Shader> shader) {
    glAttachShader(id_, shader->GetId());
    shaders_.push_back(std::move(shader));
}

void ShaderProgram::Link() {
    glLinkProgram(id_);
    CheckLinkage();
}

void ShaderProgram::Bind() const {
    glUseProgram(id_);
}

void ShaderProgram::Unbind() const {
    glUseProgram(0);
}

void ShaderProgram::CheckLinkage() {
    int linkage_status = GL_FALSE;
    glGetProgramiv(id_, GL_LINK_STATUS, &linkage_status);
    if (linkage_status != GL_TRUE) {
        int message_size;
        glGetProgramiv(id_, GL_INFO_LOG_LENGTH, &message_size);

        std::string message(message_size, 0);
        glGetProgramInfoLog(id_, message_size, nullptr, message.data());

        std::cerr << "program linkage failed:\n" << message << std::endl;
        exit(1);
    }
}

int ShaderProgram::GetUniformLocation(const std::string& name) {
    if (locations_.count(name)) {
        return locations_[name];
    }

    int location = glGetUniformLocation(id_, name.data());
    if (location == -1) {
        std::cerr << "no uniform variable " << name << std::endl;
        exit(1);
    }

    locations_[name] = location;
    return location;
}

void ShaderProgram::SetUniform(const std::string& name, const int& value) {
    int location = GetUniformLocation(name);
    glProgramUniform1i(id_, location, value);
}

void ShaderProgram::SetUniform(const std::string& name, const float& value) {
    int location = GetUniformLocation(name);
    glProgramUniform1f(id_, location, value);
}

void ShaderProgram::SetUniform(const std::string& name, const glm::vec2& value) {
    int location = GetUniformLocation(name);
    glProgramUniform2fv(id_, location, 1, glm::value_ptr(value));
}

void ShaderProgram::SetUniform(const std::string& name, const glm::vec3& value) {
    int location = GetUniformLocation(name);
    glProgramUniform3fv(id_, location, 1, glm::value_ptr(value));
}

void ShaderProgram::SetUniform(const std::string& name, const glm::vec4& value) {
    int location = GetUniformLocation(name);
    glProgramUniform3fv(id_, location, 1, glm::value_ptr(value));
}

void ShaderProgram::SetUniform(const std::string& name, const glm::mat2& value) {
    int location = GetUniformLocation(name);
    glProgramUniformMatrix2fv(id_, location, 1, GL_FALSE, glm::value_ptr(value));
}

void ShaderProgram::SetUniform(const std::string& name, const glm::mat3& value) {
    int location = GetUniformLocation(name);
    glProgramUniformMatrix3fv(id_, location, 1, GL_FALSE, glm::value_ptr(value));
}

void ShaderProgram::SetUniform(const std::string& name, const glm::mat4& value) {
    int location = GetUniformLocation(name);
    glProgramUniformMatrix4fv(id_, location, 1, GL_FALSE, glm::value_ptr(value));
}