#pragma once

#include "shader.h"

#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include <unordered_map>

class ShaderProgram {
public:
    ShaderProgram();
    ShaderProgram(std::shared_ptr<Shader> vertex_shader, std::shared_ptr<Shader> fragment_shader);
    ShaderProgram(const std::string& vertex_shader_path, const std::string& fragment_shader_path);

    void AttachShader(std::shared_ptr<Shader> shader);

    void Link();

    void Bind() const;
    void Unbind() const;

    void SetUniform(const std::string& name, const int& value);
    void SetUniform(const std::string& name, const float& value);
    void SetUniform(const std::string& name, const glm::vec2& value);
    void SetUniform(const std::string& name, const glm::vec3& value);
    void SetUniform(const std::string& name, const glm::vec4& value);
    void SetUniform(const std::string& name, const glm::mat2& value);
    void SetUniform(const std::string& name, const glm::mat3& value);
    void SetUniform(const std::string& name, const glm::mat4& value);

private:
    void CheckLinkage();
    int GetUniformLocation(const std::string& name);

private:
    unsigned int id_ = glCreateProgram();
    std::vector<std::shared_ptr<Shader>> shaders_;
    std::unordered_map<std::string, int> locations_;
};
