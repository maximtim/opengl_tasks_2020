#pragma once

#include <GL/glew.h>
#include <string>

enum class ShaderType : GLenum {
    kVertex = GL_VERTEX_SHADER,
    kFragment = GL_FRAGMENT_SHADER,
};

class Shader {
public:
    explicit Shader(ShaderType type) : type_(type), id_(glCreateShader(static_cast<GLenum>(type_))) {
    }

    ~Shader();

    void FromString(const char* code);
    void FromString(const std::string& code);
    void FromFile(const std::string& filename);

    unsigned int GetId() const;
    ShaderType GetType() const;

private:
    std::string GetTypeAsString();

    void CheckCompilation();

private:
    ShaderType type_;
    unsigned int id_;
};