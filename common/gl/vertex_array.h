#pragma once

#include "buffer.h"
#include "vertex_buffer_layout.h"

class VertexArray {
public:
    VertexArray();
    ~VertexArray();
    VertexArray(const VertexArray& rhs) = delete;
    VertexArray& operator=(const VertexArray& rhs) = delete;
    VertexArray(VertexArray&& rhs) = delete;
    VertexArray& operator=(VertexArray&& rhs) = delete;

    void AddVertexBuffer(const VertexBuffer& buffer, const VertexBufferLayout& layout);

    unsigned int GetId() const;
    void Bind() const;
    void Unbind() const;

private:
    unsigned int id_ = 0;
};