#include "texture.h"

#include <SOIL2.h>
#include <iostream>

Texture::Texture(const std::string& filepath) {
    image_ = SOIL_load_image(filepath.data(), &width_, &height_, nullptr, SOIL_LOAD_RGB);
    if (!image_) {
        std::cerr << "can't read file: " << filepath << std::endl;
        exit(1);
    }

    glGenTextures(1, &id_);
    Bind();

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width_, height_, 0, GL_RGB, GL_UNSIGNED_BYTE, image_);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glGenerateMipmap(GL_TEXTURE_2D);

    SOIL_free_image_data(image_);
}

Texture::~Texture() {
    glDeleteTextures(1, &id_);
}

void Texture::Bind(unsigned int slot) const {
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, id_);
}

void Texture::Unbind() const {
    glBindTexture(GL_TEXTURE_2D, 0);
}