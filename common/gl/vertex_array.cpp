#include "vertex_array.h"

VertexArray::VertexArray() {
    glGenVertexArrays(1, &id_);
    Bind();
}

VertexArray::~VertexArray() {
    glDeleteVertexArrays(1, &id_);
}

void VertexArray::AddVertexBuffer(const VertexBuffer& buffer, const VertexBufferLayout& layout) {
    const auto& attributes = layout.GetAttributes();

    Bind();
    buffer.Bind();
    for (size_t i = 0; i < attributes.size(); ++i) {
        glEnableVertexAttribArray(i);
        const auto& attribute = attributes[i];
        glVertexAttribPointer(i, attribute.components_count, static_cast<GLenum>(attribute.type), attribute.normalized,
                              layout.GetStride(), attribute.offset);
    }
}

unsigned int VertexArray::GetId() const {
    return id_;
}

void VertexArray::Bind() const {
    glBindVertexArray(id_);
}

void VertexArray::Unbind() const {
    glBindVertexArray(0);
}