#pragma once

#include <GL/glew.h>
#include <vector>

enum class BufferType : GLenum {
    kVertexBuffer = GL_ARRAY_BUFFER,
    kIndexBuffer = GL_ELEMENT_ARRAY_BUFFER,
};

template <BufferType type>
class Buffer {
public:
    Buffer() = default;
    Buffer(const Buffer& rhs) = delete;
    Buffer& operator=(const Buffer& rhs) = delete;
    Buffer(Buffer&& rhs) : id_(rhs.id_), size_(rhs.size_) {
        rhs.id_ = 0;
    }
    Buffer& operator=(Buffer&& rhs) {
        id_ = rhs.id_;
        size_ = rhs.size_;
        rhs.id_ = 0;
        return *this;
    }

    Buffer(const void* data, unsigned int size) {
        glGenBuffers(1, &id_);
        Bind();
        glBufferData(static_cast<GLenum>(type), size, data, GL_STATIC_DRAW);
    }

    template <typename ValueType>
    explicit Buffer(const std::vector<ValueType>& data) : Buffer(data.data(), sizeof(ValueType) * data.size()) {
        size_ = data.size();
    }

    template <typename ValueType>
    Buffer(const std::initializer_list<ValueType>& values) : Buffer(std::vector(values)) {
    }

    ~Buffer() {
        glDeleteBuffers(1, &id_);
    }

    unsigned int GetId() const {
        return id_;
    }

    unsigned int GetSize() const {
        return size_;
    }

    void Bind() const {
        glBindBuffer(static_cast<GLenum>(type), id_);
    }

    void Unbind() const {
        glBindBuffer(static_cast<GLenum>(type), 0);
    }

private:
    unsigned int id_ = 0;
    unsigned int size_ = 0;
};

using VertexBuffer = Buffer<BufferType::kVertexBuffer>;
using IndexBuffer = Buffer<BufferType::kIndexBuffer>;
