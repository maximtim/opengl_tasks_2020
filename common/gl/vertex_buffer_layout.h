#pragma once

#include <GL/glew.h>

enum class AttributeType : GLenum {
    kByte = GL_BYTE,
    kUnsignedByte = GL_UNSIGNED_BYTE,
    kShort = GL_SHORT,
    kUnsignedShort = GL_UNSIGNED_SHORT,
    kInt = GL_INT,
    kUnsignedInt = GL_UNSIGNED_INT,
    kFloat = GL_FLOAT,
    kDouble = GL_DOUBLE,
};

struct VertexAttribute {
    AttributeType type;
    unsigned int components_count;
    GLboolean normalized;
    void* offset;

    VertexAttribute(AttributeType type, unsigned int components, bool normalize, void* offset)
        : type(type), components_count(components), normalized(normalize ? GL_TRUE : GL_FALSE), offset(offset) {
    }
};

class VertexBufferLayout {
public:
    VertexBufferLayout() = default;

    unsigned int GetStride() const {
        return stride_;
    }

    const std::vector<VertexAttribute>& GetAttributes() const {
        return attributes_;
    };

    template <AttributeType type = AttributeType::kFloat>
    void AddAttribute(unsigned int components_count, bool normalize) {
        unsigned int component_size = 0;
        switch (type) {
            case AttributeType::kByte:
            case AttributeType::kUnsignedByte:
                component_size = sizeof(GLbyte);
                break;
            case AttributeType::kShort:
            case AttributeType::kUnsignedShort:
                component_size = sizeof(GLshort);
                break;
            case AttributeType::kInt:
            case AttributeType::kUnsignedInt:
                component_size = sizeof(GLint);
                break;
            case AttributeType::kFloat:
                component_size = sizeof(GLfloat);
                break;
            case AttributeType::kDouble:
                component_size = sizeof(GLdouble);
                break;
        }
        attributes_.emplace_back(type, components_count, normalize ? GL_TRUE : GL_FALSE,
                                 reinterpret_cast<void*>(stride_));
        stride_ += component_size * components_count;
    }

private:
    std::vector<VertexAttribute> attributes_;
    unsigned int stride_ = 0;
};