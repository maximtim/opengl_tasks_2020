#include "3dapplication.h"

void Abstract3dApplication::SetCameraRotationSpeed(float x_speed, float y_speed) {
    x_rot_speed_ = x_speed;
    y_rot_speed_ = y_speed;
}

void Abstract3dApplication::SetMoveSpeed(float speed) {
    move_speed_ = speed;
}

void Abstract3dApplication::UpdateState() {
    AbstractApplication::UpdateState();

    camera_.UpdateSides(window_width_, window_height_);
    UpdateCameraPosition();
    UpdateCameraDirection();
}

void Abstract3dApplication::UpdateCameraPosition() {
    if (IsPressed(Key::kW)) {
        camera_.Move(move_speed_ * camera_.GetDirection());
    }
    if (IsPressed(Key::kA)) {
        auto move_direction = glm::cross(camera_.GetDirection(), camera_.GetUpDirection());
        move_direction /= glm::length(move_direction);
        camera_.Move(-move_speed_ * move_direction);
    }
    if (IsPressed(Key::kS)) {
        camera_.Move(-move_speed_ * camera_.GetDirection());
    }
    if (IsPressed(Key::kD)) {
        auto move_direction = glm::cross(camera_.GetDirection(), camera_.GetUpDirection());
        move_direction /= glm::length(move_direction);
        camera_.Move(move_speed_ * move_direction);
    }
    if (IsPressed(Key::kR)) {
        camera_.Move(glm::vec3(0, 0, move_speed_));
    }
    if (IsPressed(Key::kF)) {
        camera_.Move(glm::vec3(0, 0, -move_speed_));
    }
}

void Abstract3dApplication::UpdateCameraDirection() {
    if (IsPressed(Key::kLeft)) {
        camera_.Rotate(0, x_rot_speed_);
    }
    if (IsPressed(Key::kRight)) {
        camera_.Rotate(0, -x_rot_speed_);
    }
    if (IsPressed(Key::kUp)) {
        camera_.Rotate(-y_rot_speed_, 0);
    }
    if (IsPressed(Key::kDown)) {
        camera_.Rotate(y_rot_speed_, 0);
    }
}