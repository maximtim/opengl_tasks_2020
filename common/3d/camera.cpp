#include "camera.h"

#include <algorithm>

#include <glm/gtc/matrix_transform.hpp>

void Camera::Move(const glm::vec3& shift) {
    position_ += shift;
}

void Camera::Rotate(float zenith, float azimuth) {
    static const float eps = 1e-3;

    direction_zenith_ += zenith;
    direction_zenith_ = std::max(direction_zenith_, eps);
    direction_zenith_ = std::min(direction_zenith_, glm::pi<float>() - eps);

    direction_azimuth_ += azimuth;
    if (direction_azimuth_ > 2 * glm::pi<float>()) {
        direction_azimuth_ -= 2 * glm::pi<float>();
    } else if (direction_azimuth_ < 0) {
        direction_azimuth_ += 2 * glm::pi<float>();
    }
}

void Camera::UpdateSides(int width, int height) {
    sides_aspect_ = static_cast<float>(width) / static_cast<float>(height);
}

glm::mat4 Camera::GetViewProjectionMatrix() const {
    return GetProjectionMatrix() * GetViewMatrix();
}

glm::vec3 Camera::GetPosition() const {
    return position_;
}

glm::vec3 Camera::GetDirection() const {
    float zenith_cos = glm::cos(direction_zenith_), zenith_sin = glm::sin(direction_zenith_);
    float azimuth_cos = glm::cos(direction_azimuth_), azimuth_sin = glm::sin(direction_azimuth_);
    return glm::vec3(zenith_sin * azimuth_cos, zenith_sin * azimuth_sin, zenith_cos);
}

glm::vec3 Camera::GetUpDirection() const {
    return glm::vec3(0., 0., 1.);
}

glm::mat4 Camera::GetViewMatrix() const {
    return glm::lookAt(position_, position_ + GetDirection(), GetUpDirection());
}

glm::mat4 Camera::GetProjectionMatrix() const {
    return glm::perspective(kViewDegrees, sides_aspect_, kNearBound, kFarBound);
}
