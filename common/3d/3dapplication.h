#include "app/application.h"
#include "camera.h"

class Abstract3dApplication : public AbstractApplication {
public:
    Abstract3dApplication(int window_width, int window_heigth, std::string window_title)
        : AbstractApplication(window_width, window_heigth, std::move(window_title)) {
    }

    void SetCameraRotationSpeed(float x_speed, float y_speed);
    void SetMoveSpeed(float speed);

protected:
    void UpdateState() override;
    virtual void UpdateCameraPosition();
    virtual void UpdateCameraDirection();

protected:
    Camera camera_;
    float move_speed_ = 0.03;
    float x_rot_speed_ = 0.01;
    float y_rot_speed_ = 0.01;
};