#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

class Camera {
public:
    Camera() = default;

    void Move(const glm::vec3& shift);
    void Rotate(float zenith, float azimuth);
    void UpdateSides(int width, int height);

    glm::mat4 GetViewProjectionMatrix() const;
    glm::vec3 GetPosition() const;
    glm::vec3 GetDirection() const;
    glm::vec3 GetUpDirection() const;

private:
    constexpr static const float kViewDegrees = 45.;
    constexpr static const float kNearBound = 0.01;
    constexpr static const float kFarBound = 100.;

    glm::mat4 GetViewMatrix() const;
    glm::mat4 GetProjectionMatrix() const;

    glm::vec3 position_{0., 0., 2.};

    float direction_zenith_ = glm::half_pi<float>();
    float direction_azimuth_ = glm::quarter_pi<float>();
    float sides_aspect_ = 1.;
};